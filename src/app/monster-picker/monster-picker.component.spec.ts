import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsterPickerComponent } from './monster-picker.component';

describe('MonsterPickerComponent', () => {
  let component: MonsterPickerComponent;
  let fixture: ComponentFixture<MonsterPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonsterPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonsterPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
