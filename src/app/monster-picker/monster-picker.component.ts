import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MonsterListService } from '../services/monster-list/monster-list.service';
import { Monster } from '../models/monster/monster';
import { ActiveMonsterService } from '../services/active-monster/active-monster.service';
import { MonsterList } from '../monster-list/model/monster-list';

@Component({
  selector: 'app-monster-picker',
  templateUrl: './monster-picker.component.html',
  styleUrls: ['./monster-picker.component.scss']
})
export class MonsterPickerComponent implements OnInit {
  public activeList: MonsterList;

  constructor(
    private activeMonsterService: ActiveMonsterService,
    private bottomSheetRef: MatBottomSheetRef<MonsterPickerComponent>,
    private monsterListService: MonsterListService,
  ) { }

  ngOnInit() {
    this.getMonsterList();
  }

  getMonsterList(): void {
    this.activeList = this.monsterListService.getActiveMonsterList();
  }

  selectMonster(event: MouseEvent, selectedMonster: Monster): void {
      this.bottomSheetRef.dismiss();
      event.preventDefault();

      this.activeMonsterService.setActiveMonster(selectedMonster);
  }
}
