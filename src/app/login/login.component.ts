import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  authError: boolean;
  md5: any;
  pbkdf2: any;

  constructor(private authService: AuthService, private router: Router) {
    this.md5 = require('md5');
    this.pbkdf2 = require('pbkdf2');
    this.authError = false;
  }

  submitLogin() {
    const usernameHash = this.md5(this.username);
    const passwordHash = this.md5(this.password);

    this.authService.getToken(usernameHash, passwordHash).then(token => {
      if (token) {
        const tokenStr = token.toString();
        this.authService.setToken(tokenStr);
        this.router.navigate(['monsters']);
      } else {
        this.authError = true;
      }
    }, () => {
      this.authError = true;
    });
  }

  ngOnInit() { }
}
