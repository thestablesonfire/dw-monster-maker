import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {FormsModule} from '@angular/forms';
import {MaterialImportsModule} from '../material-imports/material-imports.module';
import {AuthService} from '../recipe-book/services/auth/auth.service';
import {MockAuthService} from '../mocks/mock-auth.service';
import {Router} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MockRouter} from '../mocks/mock-router';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ LoginComponent ],
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                MaterialImportsModule
            ],
            providers: [
                {provide: AuthService, useClass: MockAuthService},
                { provide: Router, useClass: MockRouter }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should return and set token correctly', fakeAsync(() => {
        const tokenVal = '123TOKEN123';
        const username = 'testuser';
        const hashedUsername = '5d9c68c6c50ed3d02a2fcf54f63993b6';
        const password = 'test';
        const hashedPassword = '098f6bcd4621d373cade4e832627b4f6';
        const authService = TestBed.get(AuthService);
        const authSpy = spyOn(authService, 'getToken').and.returnValue(Promise.resolve(tokenVal));
        const authSetSpy = spyOn(authService, 'setToken');
        const routerSpy = spyOn(TestBed.get(Router), 'navigate');

        component.username = username;
        component.password = password;
        component.submitLogin();
        tick(1);

        expect(authSpy).toHaveBeenCalledTimes(1);
        expect(authSpy).toHaveBeenCalledWith(hashedUsername, hashedPassword);
        expect(authSetSpy).toHaveBeenCalledTimes(1);
        expect(authSetSpy).toHaveBeenCalledWith(tokenVal);
        expect(routerSpy).toHaveBeenCalledTimes(1);
        expect(routerSpy).toHaveBeenCalledWith(['recipes']);
    }));

    it('should handle not receiving a token', fakeAsync(() => {
        const username = 'testuser';
        const hashedUsername = '5d9c68c6c50ed3d02a2fcf54f63993b6';
        const password = 'test';
        const hashedPassword = '098f6bcd4621d373cade4e832627b4f6';
        const authService = TestBed.get(AuthService);
        const authSpy = spyOn(authService, 'getToken').and.returnValue(Promise.resolve(''));

        component.username = username;
        component.password = password;
        component.submitLogin();
        tick(1);

        expect(component.authError).toBe(true);
        expect(authSpy).toHaveBeenCalledTimes(1);
        expect(authSpy).toHaveBeenCalledWith(hashedUsername, hashedPassword);
    }));

    it('should handle the promise rejection', fakeAsync(() => {
        const username = 'testuser';
        const hashedUsername = '5d9c68c6c50ed3d02a2fcf54f63993b6';
        const password = 'test';
        const hashedPassword = '098f6bcd4621d373cade4e832627b4f6';
        const authService = TestBed.get(AuthService);
        const authSpy = spyOn(authService, 'getToken').and.returnValue(Promise.reject(''));

        component.username = username;
        component.password = password;
        component.submitLogin();
        tick(1);

        expect(component.authError).toBe(true);
        expect(authSpy).toHaveBeenCalledTimes(1);
        expect(authSpy).toHaveBeenCalledWith(hashedUsername, hashedPassword);
    }));
});
