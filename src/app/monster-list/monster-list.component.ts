import { Component, OnInit } from '@angular/core';
import { MonsterListService } from '../services/monster-list/monster-list.service';
import { MonsterList } from './model/monster-list';
import { MatDialog } from '@angular/material/dialog';
import { AddListDialogComponent } from '../add-list-dialog/add-list-dialog.component';
import { DeleteListDialogComponent } from '../delete-list-dialog/delete-list-dialog.component';

@Component({
  selector: 'app-monster-list',
  templateUrl: './monster-list.component.html',
  styleUrls: ['./monster-list.component.scss']
})
export class MonsterListComponent implements OnInit {
  public selectedList: MonsterList;
  public monsterLists: MonsterList[];

  constructor(
    private dialog: MatDialog,
    private monsterListService: MonsterListService
  ) { }

  getMonsterLists() {
    return this.monsterListService.getMonsterLists();
  }

  ngOnInit() {
    this.monsterLists = this.getMonsterLists();
    this.selectedList = this.monsterLists[0];
  }

  openAddListDialog() {
    const dialogRef = this.dialog.open(AddListDialogComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((listName) => {
      if (listName) {
        this.monsterListService.createNewList(listName);
      }
    });
  }

  openDeleteDialog(list: MonsterList) {
    const dialogRef = this.dialog.open(DeleteListDialogComponent, {
      data: list,
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((deleteList: MonsterList) => {
      if (deleteList) {
        this.monsterListService.deleteList(deleteList)
      }
    });
  }

  selectList(list: MonsterList) {
    this.selectedList = list;
  }
}
