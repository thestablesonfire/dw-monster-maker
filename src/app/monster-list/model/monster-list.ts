import { Monster } from 'src/app/models/monster/monster';

export class MonsterList {
  public name: string;
  public list: Monster[];
  private id: string;

  constructor(name?: string, list?: Monster[]) {
    this.name = name ? name : 'untitled list';
    this.list = list ? list : [];
    this.id = new Date().valueOf().toString();
  }

  addMonster(monster: Monster) {
    if (!this.monsterIsInList(monster)) {
      this.list.push(monster);
    }
  }

  getId(): string {
    return this.id;
  }

  getMonsters(): Monster[] {
    return Array.from(this.list);
  }

  monsterIsInList(monster: Monster): boolean {
    return !!this.list.find((mon) => {
      return monster.getId() === mon.getId();
    });
  }
}
