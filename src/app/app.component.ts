import { Component, ViewEncapsulation } from '@angular/core';
import { MenuService } from './menu/menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  private loaded = false;

  isMenuDisplayed = false;
  menuHasBeenToggled = false;
  title = 'Dungeon World Monster Maker';

  constructor(private menuService: MenuService) {
    this.activate();

    window.setTimeout(() => {
      this.loaded = true;
    }, 750);
  }

  private activate() {
    this.menuService.menuActionEvent.subscribe((showMenu: boolean) => {
      this.isMenuDisplayed = showMenu;
    });
  }

  toggleMenu() {
    this.isMenuDisplayed = !this.isMenuDisplayed;
    this.menuHasBeenToggled = true;
  }
}
