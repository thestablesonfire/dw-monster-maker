import { TestBed, inject } from '@angular/core/testing';

import {AuthGuardService} from './auth-guard.service';
import {AuthService} from './auth.service';
import {MockAuthService} from '../../../mocks/mock-auth.service';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {MockRouter} from '../../../mocks/mock-router';

describe('AuthGuardService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthGuardService,
                {provide: AuthService, useClass: MockAuthService},
                { provide: Router, useClass: MockRouter }
            ]
        });
    });

    it('should be created', inject([AuthGuardService], (service: AuthGuardService) => {
        expect(service).toBeTruthy();
    }));

    it('should not be able to activate the route', inject([AuthGuardService], (service: AuthGuardService) => {
        const authSpy = spyOn(TestBed.get(AuthService), 'isAuthenticated').and.returnValue(false);
        const routerSpy = spyOn(TestBed.get(Router), 'navigate');

        expect(service.canActivate()).toEqual(false);
        expect(authSpy).toHaveBeenCalledTimes(1);
        expect(routerSpy).toHaveBeenCalledTimes(1);
        expect(routerSpy).toHaveBeenCalledWith(['/login']);
    }));

    it('should be able to activate the route', inject([AuthGuardService], (service: AuthGuardService) => {
        const authSpy = spyOn(TestBed.get(AuthService), 'isAuthenticated').and.returnValue(true);

        expect(service.canActivate()).toEqual(true);
        expect(authSpy).toHaveBeenCalledTimes(1);
    }));
});
