import Timer = NodeJS.Timer;
import { CookieService } from 'ng2-cookies';
import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  private readonly cookieExpirationTime: number;
  private readonly sessionExpirationTime: number;
  private password: string;
  private username: string;
  activityHasOccurred: boolean;
  activityTimer: Timer;
  baseURL: string;
  cookieName: string;
  token: string;
  tokenUpdatedEvent: EventEmitter<null>;

  constructor(private http: HttpClient, private cookieService: CookieService, private router: Router) {
    this.baseURL = environment.baseUrl + '/token';
    this.cookieName = 'FOTG_AUTH_TOKEN';
    this.tokenUpdatedEvent = new EventEmitter();
    this.activityHasOccurred = false;
    this.sessionExpirationTime = 3600000; // 1 hr in ms
    this.cookieExpirationTime = (this.sessionExpirationTime - 60000) / (1000 * 60 * 60 * 24); // 59 min as a fraction of a day
    this.username = AuthService.getUsernameFromLocalStorage();
  }

  static hashPassword(date, password) {
    const md5 = require('md5');
    const pbkdf2 = require('pbkdf2');
    const salt = md5(date);
    const passHash = md5(password);

    return salt + pbkdf2.pbkdf2Sync(passHash, salt, 10, 32, 'sha512').toString('hex');
  }

  private static getUsernameFromLocalStorage(): string {
    return localStorage.getItem('username');
  }

  private static unsetLocalStorage(): void {
    localStorage.removeItem('username');
  }

  getToken(username: string, password: string) {
    this.username = username;
    this.password = password;

    return new Promise((resolve, reject) => {
      this.http
        .get(this.baseURL + '/' + username).subscribe(usernameResponse => {
          const usernameData = usernameResponse;

          if (usernameData.hasOwnProperty('success') && usernameData['success']) {

            this.http
              .post(this.baseURL, {
                username: username,
                password: AuthService.hashPassword(usernameData['timestamp'], password)
              }).subscribe(pwdResponse => {

                if (pwdResponse['success']) {
                  resolve(pwdResponse['token']);
                } else {
                  reject();
                }
              });
          } else {
            reject();
          }
        });
    });
  }

  getConfig(): any {
    return {
      responseType: 'json',
      headers: new HttpHeaders({
        'X-Access-Token': this.token
      }),
    }
  }

  getCookie() {
    return this.cookieService.get(this.cookieName);
  }

  setToken(token: string) {
    this.token = token;
    this.setCookie(this.cookieName, token);
    this.tokenUpdated();
  }

  validateResponse(data: any) {
    console.log(data);

    if (data && data.success) {
      return data;
    } else {
      this.showLogin();
    }

    return false;
  }

  showLogin() {
    this.router.navigate(['/login'])
  }

  isAuthenticated(): boolean {
    const token = this.getCookie();

    if (token) {
      this.setToken(token);
      this.setLocalStorage();
    } else {
      this.deleteCookie();
      AuthService.unsetLocalStorage();
    }

    return this.cookieService.check(this.cookieName);
  }

  logOut() {
    this.deleteCookie();
    AuthService.unsetLocalStorage();
    // this.app.isAuthenticated = false;
    this.activityHasOccurred = false;
    clearInterval(this.activityTimer);
    this.router.navigate(['/login']);
  }

  getUsername() {
    return this.username;
  }

  private deleteCookie() {
    this.cookieService.delete(this.cookieName);
    this.token = '';
    this.tokenUpdated();
  }

  private tokenUpdated() {
    this.tokenUpdatedEvent.emit();
  }

  private setCookie(cName: string, cValue: string) {
    this.cookieService.set(cName, cValue, this.cookieExpirationTime);

    if (!this.activityTimer) {
      this.setActivityTimer();
    }
  }

  private setActivityTimer() {
    this.activityHasOccurred = false;

    this.activityTimer = setInterval(() => {
      if (this.activityHasOccurred) {
        this.activityHasOccurred = false;
        this.getToken(this.username, this.password).then(token => {
          if (token) {
            const tokenStr = token.toString();
            this.setToken(tokenStr);
          } else {
            this.logOut();
          }
        }, () => {
          this.logOut();
        });
      } else {
        this.logOut();
      }
    }, this.sessionExpirationTime);
  }

  private setLocalStorage(): void {
    if (this.username) {
      localStorage.setItem('username', this.username);
    }
  }
}
