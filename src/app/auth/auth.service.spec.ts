import {TestBed, inject, fakeAsync} from '@angular/core/testing';

import {AuthService} from './auth.service';
import {CookieService} from 'ng2-cookies';
import {MockCookieService} from '../../../mocks/mock-cookie.service';
import {MockHttp} from '../../../mocks/mock-http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {MockRouter} from '../../../mocks/mock-router';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('AuthService', () => {
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
            ],
            providers: [
                AuthService,
                { provide: CookieService, useClass: MockCookieService },
                {provide: HttpClient, useClass: MockHttp },
                { provide: Router, useClass: MockRouter }
            ]
        });
    });

    beforeEach(inject([AuthService], (ss) => {
        service = ss;
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should run through a successfull authentication', () => {
        const md5 = require('md5');
        const http = TestBed.get(HttpClient);
        const getSpy = spyOn(http, 'get').and.returnValue(Observable.of(
            {
                'success': true,
                'username': '202cb962ac59075b964b07152d234b70',
                'timestamp': 'Mon Jan 08 2018 10:52:50 GMT-0500 (EST)'
            }
        ));

        const postSpy = spyOn(http, 'post').and.returnValue(Observable.of(
            {
                'success': true,
                'message': 'authenticated',
                'token': '123'
            }
        ));

        service.getToken('202cb962ac59075b964b07152d234b70', '81dc9bdb52d04dc20036dbd8313ed055').then(
            () => {},
            () => {}
        );

        const hashedUsername = md5('123');

        expect(getSpy).toHaveBeenCalledTimes(1);
        expect(getSpy).toHaveBeenCalledWith(service.baseURL + '/' + hashedUsername);

        expect(postSpy).toHaveBeenCalledTimes(1);
        expect(postSpy).toHaveBeenCalledWith(service.baseURL, {
            username: hashedUsername,
            password: '0dbb474670844e04d9e7a45d95d82af17cb90d45c1a63700ed19b35eb39a85626e0d5b6307737fe140057f822069d9ff'
        });
    });

    it('should reject the username', fakeAsync(() => {
        const http = TestBed.get(HttpClient);
        const getSpy = spyOn(http, 'get').and.returnValue(Observable.of(
            {'success': false}
        ));

        const test = new Promise((resolve) => {
            service.getToken('202cb962ac59075b964b07152d234b70', '14bfa6bb14875e45bba028a21ed38046').then(
                () => {
                    resolve('accepted');
                },
                () => {
                    resolve('rejected');
                }
            );
        });

        test.then(result => {
            expect(getSpy).toHaveBeenCalledTimes(1);
            expect(result).toBe('rejected');
        });
    }));

    it('should reject the password', fakeAsync(() => {
        const md5 = require('md5');
        const http = TestBed.get(HttpClient);
        const getSpy = spyOn(http, 'get').and.returnValue(Observable.of(
            {
                'success': true,
                'username': '202cb962ac59075b964b07152d234b70',
                'timestamp': 'Mon Jan 08 2018 10:52:50 GMT-0500 (EST)'
            }
        ));

        const postSpy = spyOn(http, 'post').and.returnValue(Observable.of({'success': false}));

        const test = new Promise((resolve) => {
            service.getToken('202cb962ac59075b964b07152d234b70', '81dc9bdb52d04dc20036dbd8313ed055').then(
                () => {
                    resolve('accepted');
                },
                () => {
                    resolve('rejected');
                }
            );
        });

        test.then(result => {
            expect(getSpy).toHaveBeenCalledTimes(1);
            expect(postSpy).toHaveBeenCalledTimes(1);
            expect(result).toBe('rejected');
        });
    }));

    it('should return the correct config', () => {
        service.token = '123';
        const config = service.getConfig();

        expect(config.headers['X-Access-Token']).toEqual('123');
    });

    it('should set the new token', () => {
        const setCookieSpy = spyOn(TestBed.get(CookieService), 'set');
        const tokenUpdatedSpy = spyOn(service.tokenUpdatedEvent, 'emit');

        service.setToken('123');

        expect(service.token).toEqual('123');
        expect(setCookieSpy).toHaveBeenCalledTimes(1);
        expect(tokenUpdatedSpy).toHaveBeenCalledTimes(1);
    });

    it('should set a cookie correctly', () => {
        const cookieServiceSpy = TestBed.get(CookieService);
        const setCookieSpy = spyOn(cookieServiceSpy, 'set');

        service.cookieName = 'test';
        service.setToken('123');

        expect(setCookieSpy).toHaveBeenCalledTimes(1);
        expect(setCookieSpy).toHaveBeenCalledWith('test', '123', 0.04097222222222222);
    });

    it('should get a cookie correctly', () => {
        const getSpy = spyOn(TestBed.get(CookieService), 'get').and.returnValue('123');

        service.cookieName = 'test';
        service.setToken('123');
        const token = service.getCookie();

        expect(token).toEqual('123');
    });

    it('should show the login modal', () => {
            service.app = {showLoginDialog: function () {}};
            const spy = spyOn(service.app, 'showLoginDialog');

            service.showLogin();

            expect(spy).toHaveBeenCalledTimes(1);
        }
    );

    it('should validate the response', () => {
        const response = {success: true, data: 'datah'};

        const data = service.validateResponse(response);

        expect(data.data).toBe('datah');
    });

    it('should not validate the response', () => {
        service.app = {showLoginDialog: function () {}};
        const spy = spyOn(service.app, 'showLoginDialog');
        const response = {success: false, data: 'datah'};

        const data = service.validateResponse(response);

        expect(data).toBe(false);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should verify that the user is authenticated', () => {
        const cookieService = TestBed.get(CookieService);
        const getSpy = spyOn(cookieService, 'get').and.returnValue('123');
        const setTokenSpy = spyOn(service, 'setToken');
        const cookeCheck = spyOn(cookieService, 'check').and.returnValue(true);

        service.app = {isAuthenticated: false};
        service.cookieName = 'test';
        service.setToken('123');

        expect(service.isAuthenticated()).toBe(true);
        expect(service.app.isAuthenticated).toBe(true);
        expect(setTokenSpy).toHaveBeenCalledTimes(2);
        expect(getSpy).toHaveBeenCalledTimes(1);
        expect(cookeCheck).toHaveBeenCalledTimes(1);
    });

    it('should verify that the user is NOT authenticated', () => {
        const cookieService = TestBed.get(CookieService);
        const getSpy = spyOn(cookieService, 'get').and.returnValue('');
        const cookeCheck = spyOn(cookieService, 'check').and.returnValue(false);

        service.app = {isAuthenticated: true};
        service.cookieName = 'test';

        expect(service.isAuthenticated()).toBe(false);
        expect(getSpy).toHaveBeenCalledTimes(1);
        expect(cookeCheck).toHaveBeenCalledTimes(1);
        expect(service.token).toBe('');
    });

    it('should log the user out', () => {
        const routerSpy = spyOn(TestBed.get(Router), 'navigate');
        service.app = {isAuthenticated: true};

        service.logOut();

        expect(service.app.isAuthenticated).toBe(false);
        expect(service.activityHasOccurred).toBe(false);
        expect(service.activityTimer).toBe(undefined);
        expect(routerSpy).toHaveBeenCalledTimes(1);
        expect(routerSpy).toHaveBeenCalledWith(['/login']);
    });

    it('should return username', () => {
        service.username = '123hash';
        expect(service.getUsername()).toEqual('123hash');
    });

    it('should set local storage', () => {
        const localSpy = spyOn(localStorage, 'setItem');
        const cookieSpy = spyOn(service, 'getCookie').and.returnValue('123');
        service.app = {isAuthenticated: true};
        service.username = 'testUser';

        service.isAuthenticated();

        expect(localSpy).toHaveBeenCalledWith('username', 'testUser');
        expect(cookieSpy).toHaveBeenCalledTimes(1);

        service.username = '';

        service.isAuthenticated();
        expect(cookieSpy).toHaveBeenCalledTimes(2);
        expect(localSpy).toHaveBeenCalledTimes(1);
    });

    it('should test setActivityTimer', () => {
        // TODO
    });
});
