import { TestBed } from '@angular/core/testing';

import { MonsterFactory } from './monster.factory';

describe('MonsterFactory', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonsterFactory = TestBed.get(MonsterFactory);
    expect(service).toBeTruthy();
  });
});
