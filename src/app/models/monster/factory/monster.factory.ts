import { Injectable } from '@angular/core';
import { Monster } from '../monster';

@Injectable({
  providedIn: 'root'
})
export class MonsterFactory {

  constructor() { }

  createNewMonster(): Monster {
    return new Monster();
  }

  createMonsterFromJSON(monsterJSON: JSON) {
    console.log(monsterJSON);
  }
}
