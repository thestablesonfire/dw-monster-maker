import { TagModifier } from './modifier/tag-modifier';
import { MonsterAttributes } from '../attribute/monster-attributes.enum';

export class Tag {
    constructor(
        public description: string,
        public modifier: TagModifier,
        public attribute?: MonsterAttributes
    ) {}
}
