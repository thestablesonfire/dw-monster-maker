import { TagModifier } from '../modifier/tag-modifier';
import { Tag } from '../tag';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

export class MonsterTag extends Tag {
    constructor(description: string, value: string) {
        super(description, new TagModifier(ModifierOperation.TEXT, value));
    }
}
