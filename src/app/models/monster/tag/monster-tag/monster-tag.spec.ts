import { MonsterTag } from './monster-tag';
import { TagModifier } from '../modifier/tag-modifier';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

describe('MonsterTag', () => {
    it('should create an instance', () => {
        expect(new MonsterTag('monster tag', 'operation')).toBeTruthy();
    });
});
