import { Injectable } from '@angular/core';
import { MonsterTag } from './monster-tag';
import { TagModifier } from '../modifier/tag-modifier';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

@Injectable({
    providedIn: 'root'
})
export class MonsterTagService {
    private monsterTags: MonsterTag[];

    constructor() {
        this.monsterTags = [
            new MonsterTag('Devious', 'Its main danger lies beyond the simple clash of battle.'),
            new MonsterTag('Amorphous', 'Its anatomy and organs are bizarre and unnatural.'),
            new MonsterTag('Organized', 'It has a group structure that aids it in survival. ' +
                'Defeating one may cause the wrath of others. One may sound an alarm.'),
            new MonsterTag('Intelligent', 'It’s smart enough that some individuals pick up other' +
                ' skills. The GM can adapt the monster by adding tags to reflect specific training, like a mage or warrior.'),
            new MonsterTag('Hoarder', 'It almost certainly has treasure.'),
            new MonsterTag('Stealthy', 'It can avoid detection and prefers to attack with the' +
                ' element of surprise.'),
            new MonsterTag('Terrifying', 'Its presence and appearance evoke fear.'),
            new MonsterTag('Cautious', 'It prizes survival over aggression.'),
            new MonsterTag('Construct', 'It was made, not born.'),
            new MonsterTag('Planar', 'It’s from beyond this world.'),
        ];
    }

    getAllMonsterTags(): MonsterTag[] {
        return this.monsterTags.slice();
    }

    getAmorphousTag(): MonsterTag {
        return new MonsterTag('Amorphous', 'Its anatomy and organs are bizarre and unnatural.');
    }

    getCautiousTag(): MonsterTag {
        return new MonsterTag('Cautious', 'It prizes survival over aggression.');
    }

    getConstructTag(): MonsterTag {
        return new MonsterTag('Construct', 'It was made, not born.');
    }

    getDeviousTag(): MonsterTag {
        return new MonsterTag('Devious', 'Its main danger lies beyond the simple clash of battle.');
    }

    getGroupTag(): MonsterTag {
        return new MonsterTag('Group', 'In small groups, about 2–5.');
    }

    getHoarderTag(): MonsterTag {
        return new MonsterTag('Hoarder', 'It almost certainly has treasure.');
    }

    getIntelligentTag(): MonsterTag {
        return new MonsterTag('Intelligent', 'It’s smart enough that some individuals pick up other' +
            ' skills. The GM can adapt the monster by adding tags to reflect specific training, like a mage or warrior.');
    }

    getHordeTag(): MonsterTag {
        return new MonsterTag('Horde', 'In large groups.');
    }

    getMagicalTag(): MonsterTag {
        return new MonsterTag('Magical', 'It is by nature magical through and through.');
    }

    getOrganizedTag(): MonsterTag {
        return new MonsterTag('Organized', 'It has a group structure that aids it in survival. ' +
            'Defeating one may cause the wrath of others. One may sound an alarm.');
    }

    getPlanarTag(): MonsterTag {
        return new MonsterTag('Planar', 'It’s from beyond this world.');
    }

    getSolitaryTag(): MonsterTag {
        return new MonsterTag('Solitary', 'All by its lonesome.');
    }

    getStealthyTag(): MonsterTag {
        return new MonsterTag('Stealthy', 'It can avoid detection and prefers to attack with the' +
            ' element of surprise.');
    }

    getTerrifyingTag(): MonsterTag {
        return new MonsterTag('Terrifying', 'Its presence and appearance evoke fear.');
    }

    getTinyTag(): MonsterTag {
        return new MonsterTag('Tiny', 'Smaller than a house cat');
    }
}
