import { TestBed } from '@angular/core/testing';

import { MonsterTagService } from './monster-tag.service';

describe('MonsterTagService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: MonsterTagService = TestBed.get(MonsterTagService);
        expect(service).toBeTruthy();
    });
});
