import { Injectable } from '@angular/core';
import { WeaponTag } from './weapon-tag';
import { TagModifier } from '../modifier/tag-modifier';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';
import { MonsterAttributes } from '../../attribute/monster-attributes.enum';
import { Tag } from '../tag';

@Injectable({
    providedIn: 'root'
})
export class WeaponTagService {
    constructor() {
    }

    getAllWeaponTags() {
        return [
            this.getAmmoTag(0),
            this.getCloseTag(),
            this.getFarTag(),
            this.getForcefulTag(),
            this.getHandTag(),
            this.getIgnoresArmorTag(),
            this.getMessyTag(),
            this.getNearTag(),
            this.getPiercingTag(0),
            this.getPlusDamageTag(0),
            this.getPreciseTag(),
            this.getReachTag(),
            this.getReloadTag(),
            this.getStunnedTag(),
            this.getThrownTag()
        ];
    }

    getAmmoTag(ammoNum: number): WeaponTag {
        return new WeaponTag(
            'Ammo',
            new TagModifier(ModifierOperation.PLUS, ammoNum),
            MonsterAttributes.AMMO
        );
    }

    getCloseTag(): WeaponTag {
        return new WeaponTag(
            'Close',
            new TagModifier(
                ModifierOperation.TEXT,
                'It’s useful for attacking something at arm’s reach plus a foot or two.'
            )
        );
    }

    getFarTag(): WeaponTag {
        return new WeaponTag(
            'Far',
            new TagModifier(
                ModifierOperation.TEXT,
                'It’s useful for attacking something in shouting distance.'
            )
        );
    }

    getForcefulTag(): WeaponTag {
        return new WeaponTag(
            'Forceful',
            new TagModifier(
                ModifierOperation.TEXT,
                'It can knock someone back a pace, maybe even off their feet.'
            )
        );
    }

    getHandTag(): WeaponTag {
        return new WeaponTag(
            'Hand',
            new TagModifier(
                ModifierOperation.TEXT,
                'It’s useful for attacking something within your reach, no further.'
            )
        );
    }

    getIgnoresArmorTag(): WeaponTag {
        return new WeaponTag(
            'Ignores Armor',
            new TagModifier(
                ModifierOperation.TEXT,
                'Don’t subtract armor from the damage taken.'
            )
        );
    }

    getMessyTag(): WeaponTag {
        return new WeaponTag(
            'Messy',
            new TagModifier(
                ModifierOperation.TEXT,
                'It does damage in a particularly destructive way, ripping people and things apart.'
            )
        );
    }

    getNearTag() {
        return new WeaponTag(
            'Near',
            new TagModifier(
                ModifierOperation.TEXT,
                'It’s useful for attacking if you can see the whites of their eyes.'
            )
        );
    }

    getPiercingTag(pierceVal: number): WeaponTag {
        return new WeaponTag(
            'piercing',
            new TagModifier(
                ModifierOperation.PLUS,
                pierceVal
            ),
            MonsterAttributes.PIERCE
        );
    }

    getPlusDamageTag(addDamage: number): WeaponTag {
        const modifier = addDamage > 0 ? '+' : '';

        return new WeaponTag(
            'damage',
            new TagModifier(ModifierOperation.PLUS, addDamage),
            MonsterAttributes.DAMAGE
        );
    }

    getPreciseTag(): WeaponTag {
        return new WeaponTag(
            'Precise',
            new TagModifier(
                ModifierOperation.TEXT,
                'It rewards careful strikes. You use DEX to hack and slash with this weapon, not STR.'
            )
        );
    }

    getReachTag(): WeaponTag {
        return new WeaponTag(
            'Reach',
            new TagModifier(
                ModifierOperation.TEXT,
                'It’s useful for attacking something that’s several feet away—maybe as far as ten.'
            )
        );
    }

    getReloadTag(): WeaponTag {
        return new WeaponTag(
            'Reload',
            new TagModifier(
                ModifierOperation.TEXT,
                'After you attack with it, it takes more than a moment to reset for another attack.'
            ),
        );
    }

    getStunnedTag(): WeaponTag {
        return new WeaponTag(
            'Stun',
            new TagModifier(
                ModifierOperation.TEXT,
                'When you attack with it, it does stun damage instead of normal damage.'
            )
        );
    }

    getThrownTag(): WeaponTag {
        return new WeaponTag(
            'Thrown',
            new TagModifier(
                ModifierOperation.TEXT,
                'Throw it at someone to hurt them. If you volley with this' +
                ' weapon, you can’t choose to mark off ammo on a 7–9; once you throw it, it’s gone until you can recover it.'
            )
        );
    }
}
