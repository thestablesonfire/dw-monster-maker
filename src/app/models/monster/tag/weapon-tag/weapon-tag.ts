import { Tag } from '../tag';
import { TagModifier } from '../modifier/tag-modifier';
import { MonsterAttributes } from '../../attribute/monster-attributes.enum';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

export class WeaponTag extends Tag {
    constructor(
        description: string,
        modifier: TagModifier,
        attribute?: MonsterAttributes
    ) {
        super(description, modifier, attribute);
    }

    getWeaponTagString() {
        const val = this.modifier.operation === ModifierOperation.PLUS ? this.modifier.value : null;

        return val ? val + ' ' + this.description : this.description;
    }

    updateModifierValue(value: number) {
        this.modifier.value = value;
    }
}
