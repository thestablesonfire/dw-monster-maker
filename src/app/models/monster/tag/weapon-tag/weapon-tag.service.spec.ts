import { TestBed } from '@angular/core/testing';

import { WeaponTagService } from './weapon-tag.service';

describe('WeaponTagService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: WeaponTagService = TestBed.get(WeaponTagService);
        expect(service).toBeTruthy();
    });
});
