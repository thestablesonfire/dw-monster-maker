import { Tag } from './tag';
import { TagModifier } from './modifier/tag-modifier';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

describe('Tag', () => {
  it('should create an instance', () => {
    expect(new Tag('descrption', new TagModifier(ModifierOperation.TEXT))).toBeTruthy();
  });
});
