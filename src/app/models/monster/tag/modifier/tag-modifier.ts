import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

export class TagModifier {
    constructor(
        public operation: ModifierOperation,
        public value?: any
    ) {}
}
