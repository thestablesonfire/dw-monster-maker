import { TagModifier } from './tag-modifier';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';

describe('Modifier', () => {
    it('should create an instance', () => {
        expect(new TagModifier(ModifierOperation.TEXT, 'test')).toBeTruthy();
    });
});
