export enum ModifierOperation {
    PLUS = 'plus',
    NONE = 'none',
    TEXT = 'text'
}
