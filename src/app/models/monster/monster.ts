import { MonsterSize } from './size/monster-size';
import { MonsterGroupSize } from './group-size/monster-group-size';
import { MonsterTag } from './tag/monster-tag/monster-tag';
import { MonsterDefense } from './defense/monster-defense';
import { WeaponTag } from './tag/weapon-tag/weapon-tag';
import { DamageValueService } from './damageValue/die-size/damage-value.service';
import { DieSizes } from './damageValue/die-size/die-sizes.enum';
import { DamageValue } from './damageValue/damage-value';
import { WeaponTagService } from './tag/weapon-tag/weapon-tag.service';
import { ModifierOperation } from 'src/app/models/monster/tag/modifier/modifier-operation.enum';
import { MonsterAttributes } from './attribute/monster-attributes.enum';
import { MonsterTagService } from './tag/monster-tag/monster-tag.service';

export class Monster {
    public name: string;
    public description: string;
    public attackName: string;
    public instinct: string;
    public groupSize: MonsterGroupSize;
    public size: MonsterSize;
    public defenses: MonsterDefense;
    public specialQuality: string;
    public moves: string[];
    public monsterTags: MonsterTag[];
    public weaponTags: WeaponTag[];
    private id: string;

    constructor() {
        this.moves = [];
        this.monsterTags = [];
        this.weaponTags = [];
        this.generateId();
    }

    addMove(move: string) {
        this.moves.push(move);
    }

    addMonsterTag(monsterTag: MonsterTag) {
        this.monsterTags.push(monsterTag);
    }

    addWeaponTag(weaponTag: WeaponTag) {
        this.weaponTags.push(weaponTag);
    }

    deleteMonsterTag(index: number) {
        this.monsterTags.splice(index, 1);
    }

    deleteMove(index: number) {
        this.moves.splice(index, 1);
    }

    deleteWeaponTag(index: number) {
        this.weaponTags.splice(index, 1);
    }

    getArmor() {
        return this.defenses ? this.defenses.getArmor() : '';
    }

    getDamageValue(
        damageValueService: DamageValueService,
        weaponTagService: WeaponTagService
    ): DamageValue {
        const damage = this.groupSize ? damageValueService.getDamageValue(this.groupSize.getAttackDie()) :
            damageValueService.getDamageValue(DieSizes.D0);

        if (this.size) {
            damage.applyModifier(this.getWeaponTagDamageModifier(weaponTagService));
        }

        return damage;
    }

    getHP() {
        let hp = 0;

        hp += this.groupSize ? this.groupSize.getHP() : 0;
        hp += this.size ? this.size.getHP() : 0;

        return hp;
    }

    getId(): string {
      return this.id;
    }

    getMonsterTags(monsterTagService: MonsterTagService): string {
        let tagString = '';
        let tags = this.monsterTags;
        tags = tags.concat(this.size ? this.size.getMonsterTags(monsterTagService) : []);
        tags = tags.concat(this.groupSize ? this.groupSize.getMonsterTags(monsterTagService) : []);

        for (const tag of tags) {
            tagString += tag.description;
            tagString += tag === tags[tags.length - 1] ? '' : ', ';
        }

        return tagString;
    }

    getWeaponTags(weaponTagService: WeaponTagService): string {
        let tagString = '';
        let tags = this.size ? this.size.getWeaponTags(weaponTagService) : [];
        tags = tags.concat(this.weaponTags);

        for (const tag of tags) {
            tagString += tag.getWeaponTagString();
            tagString += tag === tags[tags.length - 1] ? '' : ', ';
        }

        return tagString;
    }

    private generateId(): void {
      this.id = new Date().valueOf().toString();
    }

    private getWeaponTagDamageModifier(weaponTagService: WeaponTagService): number {
        let dmgVal = 0;
        let tags = this.size.getWeaponTags(weaponTagService);

        tags = tags.concat(this.weaponTags);
        tags = tags.filter((tag) => {
            return tag.modifier.operation === ModifierOperation.PLUS &&
                tag.attribute === MonsterAttributes.DAMAGE;
        });

        for (const tag of tags) {
            dmgVal += tag.modifier.value;
        }

        return dmgVal;
    }
}
