import { DieSizes } from './die-size/die-sizes.enum';

export class DamageValue {
    private modifier: number;

    constructor(private dieSize: DieSizes) {
        this.modifier = 0;
    }

    applyModifier(mod: number) {
        this.modifier += mod;
    }

    getDamageString(): string {
        let damageString = '(' + this.dieSize.toString();
        const modSign = this.modifier >= 0 ? '+' : '';

        damageString += this.modifier ? modSign + this.modifier : '';
        damageString += ' damage)';

        return damageString;
    }
}
