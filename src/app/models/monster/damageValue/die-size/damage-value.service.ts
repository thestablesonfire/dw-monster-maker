import { Injectable } from '@angular/core';
import { DieSizes } from './die-sizes.enum';
import { DamageValue } from '../damage-value';

@Injectable({
    providedIn: 'root'
})
export class DamageValueService {

    constructor() {}

    getDamageValue(size: DieSizes): DamageValue {
        return new DamageValue(size);
    }
}
