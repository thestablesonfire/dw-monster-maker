import { TestBed } from '@angular/core/testing';

import { DieSizeService } from './damage-value.service';

describe('DieSizeService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: DieSizeService = TestBed.get(DieSizeService);
        expect(service).toBeTruthy();
    });
});
