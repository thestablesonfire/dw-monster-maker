export enum DieSizes {
    D0 = 'd0',
    D3 = 'd3',
    D4 = 'd4',
    D6 = 'd6',
    D8 = 'd8',
    D10 = 'd10',
    D12 = 'd12',
    D20 = 'd20'
}
