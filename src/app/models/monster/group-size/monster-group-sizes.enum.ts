export enum MonsterGroupSizes {
    LONE = 'lone',
    SMALL_GROUPS = 'small_groups',
    LARGE_GROUPS = 'large_groups'
}
