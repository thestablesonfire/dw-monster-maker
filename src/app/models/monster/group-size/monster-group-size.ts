import { MonsterGroupSizes } from './monster-group-sizes.enum';
import { DieSizes } from '../damageValue/die-size/die-sizes.enum';
import { MonsterTagService } from '../tag/monster-tag/monster-tag.service';
import { MonsterTag } from '../tag/monster-tag/monster-tag';

export class MonsterGroupSize {
    constructor(
        public key: MonsterGroupSizes,
        public description: string,
        public tags: any[]
    ) {}

    getAttackDie(): DieSizes {
        let attack: DieSizes;

        switch (this.key) {
            case MonsterGroupSizes.LONE:
                attack = DieSizes.D10;
                break;
            case MonsterGroupSizes.SMALL_GROUPS:
                attack = DieSizes.D8;
                break;
            case MonsterGroupSizes.LARGE_GROUPS:
                attack = DieSizes.D6;
                break;
        }

        return attack;
    }

    getHP() {
        let hp = 0;

        switch (this.key) {
            case MonsterGroupSizes.LONE:
                hp = 12;
                break;
            case MonsterGroupSizes.SMALL_GROUPS:
                hp = 6;
                break;
            case MonsterGroupSizes.LARGE_GROUPS:
                hp = 3;
                break;
        }

        return hp;
    }

    getMonsterTags(monsterTagService: MonsterTagService): MonsterTag[] {
        const tags = [];

        switch (this.key) {
            case MonsterGroupSizes.LONE:
                tags.push(monsterTagService.getSolitaryTag());
                break;
            case MonsterGroupSizes.SMALL_GROUPS:
                tags.push(monsterTagService.getGroupTag());
                break;
            case MonsterGroupSizes.LARGE_GROUPS:
                tags.push(monsterTagService.getHordeTag());
                break;
        }

        return tags;
    }
}
