import { TestBed } from '@angular/core/testing';

import { MonsterGroupSizeService } from './monster-group-size.service';

describe('GroupSizeService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: MonsterGroupSizeService = TestBed.get(MonsterGroupSizeService);
        expect(service).toBeTruthy();
    });
});
