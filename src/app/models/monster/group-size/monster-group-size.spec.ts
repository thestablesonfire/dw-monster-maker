import { MonsterGroupSize } from './monster-group-size';
import { MonsterGroupSizes } from './monster-group-sizes.enum';

describe('MonsterGroupSize', () => {
    it('should create an instance', () => {
        expect(new MonsterGroupSize(MonsterGroupSizes.LONE, 'Solitary', [])).toBeTruthy();
    });
});
