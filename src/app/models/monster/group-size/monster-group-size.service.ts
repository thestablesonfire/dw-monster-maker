import { Injectable } from '@angular/core';
import { MonsterGroupSizes } from './monster-group-sizes.enum';
import { MonsterGroupSize } from './monster-group-size';

@Injectable({
    providedIn: 'root'
})
export class MonsterGroupSizeService {
    private groupSizes: MonsterGroupSize[];

    constructor() {
        this.groupSizes = [
            new MonsterGroupSize(MonsterGroupSizes.LONE, 'Lone', []),
            new MonsterGroupSize(MonsterGroupSizes.SMALL_GROUPS, 'Small Groups', []),
            new MonsterGroupSize(MonsterGroupSizes.LARGE_GROUPS, 'Large Groups', []),
        ];
    }

    getGroupSizes(): MonsterGroupSize[] {
        return this.groupSizes.slice();
    }
}
