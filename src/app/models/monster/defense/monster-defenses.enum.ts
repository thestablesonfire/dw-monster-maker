export enum MonsterDefenses {
    CLOTH_FLESH = 'cloth_flesh',
    LEATHER_HIDE = 'leather_hide',
    MAIL_SCALES = 'mail_scales',
    PLATE_BONE = 'plate_bone',
    MAGICAL = 'magical'
}
