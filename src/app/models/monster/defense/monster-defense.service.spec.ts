import { TestBed } from '@angular/core/testing';

import { MonsterDefenseService } from './monster-defense.service';

describe('MonsterDefenseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonsterDefenseService = TestBed.get(MonsterDefenseService);
    expect(service).toBeTruthy();
  });
});
