import { Injectable } from '@angular/core';
import { MonsterDefense } from './monster-defense';
import { MonsterDefenses } from './monster-defenses.enum';

@Injectable({
    providedIn: 'root'
})
export class MonsterDefenseService {
    private defenses: MonsterDefense[];

    constructor() {
        this.defenses = [
            new MonsterDefense(MonsterDefenses.CLOTH_FLESH, 'Cloth or Flesh', []),
            new MonsterDefense(MonsterDefenses.LEATHER_HIDE, 'Leather or Hide', []),
            new MonsterDefense(MonsterDefenses.MAIL_SCALES, 'Mail or Scales', []),
            new MonsterDefense(MonsterDefenses.PLATE_BONE, 'Plate or Bone', []),
            new MonsterDefense(MonsterDefenses.MAGICAL, 'Permanent Magical Armor', []),
        ];
    }

    getDefenses() {
        return this.defenses.slice();
    }
}
