import { MonsterDefense } from './monster-defense';
import { MonsterDefenses } from './monster-defenses.enum';

describe('MonsterDefense', () => {
  it('should create an instance', () => {
    expect(new MonsterDefense(MonsterDefenses.CLOTH_FLESH, 'cloth', [])).toBeTruthy();
  });
});
