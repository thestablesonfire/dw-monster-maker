import { MonsterDefenses } from './monster-defenses.enum';
import { MonsterTag } from '../tag/monster-tag/monster-tag';

export class MonsterDefense {
    constructor(
        public key: MonsterDefenses,
        public description: string,
        public tags: MonsterTag[]
    ) {}

    getArmor(): number {
        let armor;

        switch (this.key) {
            case MonsterDefenses.CLOTH_FLESH:
                armor = 0;
                break;
            case MonsterDefenses.LEATHER_HIDE:
                armor = 1;
                break;
            case MonsterDefenses.MAIL_SCALES:
                armor = 2;
                break;
            case MonsterDefenses.PLATE_BONE:
                armor = 3;
                break;
            case MonsterDefenses.MAGICAL:
                armor = 4;
                break;
            default:
                armor = 0;
                break;
        }

        return armor;
    }
}
