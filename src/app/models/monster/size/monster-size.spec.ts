import { MonsterSize } from './monster-size';
import { MonsterSizes } from './monster-sizes.enum';

describe('MonsterSize', () => {
    it('should create an instance', () => {
        expect(new MonsterSize(MonsterSizes.SMALL, 'small', [])).toBeTruthy();
    });
});
