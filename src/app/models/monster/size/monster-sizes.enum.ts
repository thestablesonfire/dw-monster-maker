export enum MonsterSizes {
    TINY = 'tiny',
    SMALL = 'small',
    NORMAL = 'normal',
    LARGE = 'large',
    HUGE = 'huge'
}
