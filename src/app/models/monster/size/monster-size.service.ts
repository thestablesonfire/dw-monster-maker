import { Injectable } from '@angular/core';
import { MonsterSize } from './monster-size';
import { MonsterSizes } from './monster-sizes.enum';

@Injectable({
    providedIn: 'root'
})
export class MonsterSizeService {
    private sizes: MonsterSize[];

    constructor() {
        this.sizes = [
            new MonsterSize(MonsterSizes.TINY, 'Tiny', []),
            new MonsterSize(MonsterSizes.SMALL, 'Small', []),
            new MonsterSize(MonsterSizes.NORMAL, 'Normal', []),
            new MonsterSize(MonsterSizes.LARGE, 'Large', []),
            new MonsterSize(MonsterSizes.HUGE, 'Huge', [])
        ];
    }

    getSizes(): MonsterSize[] {
        return this.sizes.slice();
    }
}
