import { MonsterTag } from '../tag/monster-tag/monster-tag';
import { MonsterSizes } from './monster-sizes.enum';
import { WeaponTag } from '../tag/weapon-tag/weapon-tag';
import { WeaponTagService } from '../tag/weapon-tag/weapon-tag.service';
import { MonsterTagService } from '../tag/monster-tag/monster-tag.service';

export class MonsterSize {
    constructor(
        public key: MonsterSizes,
        public description: string,
        public tags: MonsterTag[]
    ) {}

    getHP() {
        let hp = 0;

        switch (this.key) {
            case MonsterSizes.LARGE:
                hp = 4;
                break;
            case MonsterSizes.HUGE:
                hp = 8;
                break;
        }

        return hp;
    }

    getMonsterTags(monsterTagService: MonsterTagService): MonsterTag[] {
        const tags = [];

        switch (this.key) {
            case MonsterSizes.TINY:
                tags.push(monsterTagService.getTinyTag());
        }

        return tags;
    }

    getWeaponTags(weaponTagService: WeaponTagService): WeaponTag[] {
        const tags = [];

        switch (this.key) {
            case MonsterSizes.TINY:
                tags.push(weaponTagService.getHandTag());
                tags.push(weaponTagService.getPlusDamageTag(-2));
                break;
            case MonsterSizes.SMALL:
                tags.push(weaponTagService.getCloseTag());
                break;
            case MonsterSizes.NORMAL:
                tags.push(weaponTagService.getCloseTag());
                break;
            case MonsterSizes.LARGE:
                tags.push(weaponTagService.getCloseTag());
                tags.push(weaponTagService.getReachTag());
                tags.push(weaponTagService.getPlusDamageTag(1));
                break;
            case MonsterSizes.HUGE:
                tags.push(weaponTagService.getReachTag());
                tags.push(weaponTagService.getPlusDamageTag(3));
                break;
        }

        return tags;
    }
}
