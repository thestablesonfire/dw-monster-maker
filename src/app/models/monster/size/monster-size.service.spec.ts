import { TestBed } from '@angular/core/testing';

import { MonsterSizeService } from './monster-size.service';

describe('MonsterSizeService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: MonsterSizeService = TestBed.get(MonsterSizeService);
        expect(service).toBeTruthy();
    });
});
