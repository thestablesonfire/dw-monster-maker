import { AuthService } from 'src/app/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Monster } from 'src/app/models/monster/monster';
import { MonsterList } from 'src/app/monster-list/model/monster-list';
import { environment } from '../../../environments/environment';
import { MonsterFactory } from 'src/app/models/monster/factory/monster.factory';

@Injectable({
  providedIn: 'root'
})
export class MonsterListService {
  @Output() activeListChangedEvent = new EventEmitter<null>();
  private activeMonsterList: MonsterList;
  private endpointURL = environment.baseUrl + '/monsterList/';
  private monsterLists: MonsterList[];

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private monsterFactory: MonsterFactory,
  ) {
    this.requestMonsterLists();
  }

  createNewList(listName: string) {
    this.monsterLists.push(
      new MonsterList(listName)
    );

    if (this.monsterLists.length === 1) {
      this.activeMonsterList = this.monsterLists[0];
    }
  }

  deleteList(monsterList: MonsterList) {
    const index = this.findListIndex(monsterList);

    this.monsterLists.splice(index, 1);

    if (this.findListIndex(this.activeMonsterList) === -1 && this.monsterLists.length) {
      this.activeMonsterList = this.monsterLists[0];
    }
  }

  getActiveMonsterList(): MonsterList {
    return this.activeMonsterList;
  }

  getMonsterLists(): MonsterList[] {
    return this.monsterLists;
  }

  saveMonsterToList(activeMonster: Monster) {
    this.activeMonsterList.addMonster(activeMonster);
    this.saveList(this.activeMonsterList);
  }

  private findListIndex(list: MonsterList): number {
    return this.monsterLists.findIndex((thisList) => {
      return list.getId() === thisList.getId();
    });
  }

  private requestMonsterLists() {
    this.http.get(
      this.endpointURL + this.authService.getUsername(),
      this.authService.getConfig(),
    ).subscribe(response => {
      console.log(response);
    })
  }

  private saveList(list: MonsterList) {
    this.http.post(
      this.endpointURL + this.authService.getUsername(),
      {
        list: list
      },
      this.authService.getConfig()
    ).subscribe(response => {
      console.log(response);
    });
  }
}
