import { Injectable, Output, EventEmitter } from '@angular/core';
import { Monster } from '../../models/monster/monster';
import { MonsterListService } from '../monster-list/monster-list.service';
import { MonsterFactory } from 'src/app/models/monster/factory/monster.factory';

@Injectable({
  providedIn: 'root'
})
export class ActiveMonsterService {
  @Output() activeMonsterChangedEvent: EventEmitter<null>;
  private activeMonster: Monster;

  constructor(
    private monsterFactory: MonsterFactory,
    private monsterListService: MonsterListService
  ) {
    this.activeMonsterChangedEvent = new EventEmitter();
  }

  getActiveMonster(): Monster {
    return this.activeMonster;
  }

  saveMonsterToList() {
    this.monsterListService.saveMonsterToList(this.activeMonster);
  }

  setActiveMonster(monster: Monster) {
    this.activeMonster = monster;
    this.activeMonsterChangedEvent.emit();
  }

  setNewActiveMonster() {
    this.activeMonster = this.monsterFactory.createNewMonster();
    this.activeMonsterChangedEvent.emit();

    return this.activeMonster;
  }
}
