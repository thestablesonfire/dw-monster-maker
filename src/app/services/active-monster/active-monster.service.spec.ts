import { TestBed } from '@angular/core/testing';

import { ActiveMonsterService } from './active-monster.service';

describe('ActiveMonsterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActiveMonsterService = TestBed.get(ActiveMonsterService);
    expect(service).toBeTruthy();
  });
});
