import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class MenuService {
    menuActionEvent: EventEmitter<boolean>;

    constructor() {
        this.menuActionEvent = new EventEmitter();
    }
}
