import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from './menu.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    constructor(
        private menuService: MenuService,
        private router: Router
    ) {}

    ngOnInit() {}

    navigateTo(menuRoute: string) {
        this.menuService.menuActionEvent.emit(false);
        this.router.navigate([menuRoute]);
    }
}
