import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonsterMakerComponent } from './monster-maker/monster-maker.component';
import { MonsterListComponent } from './monster-list/monster-list.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'monsters',
        canActivate: [AuthGuardService],
        component: MonsterMakerComponent
    },
    {
        path: 'lists',
        canActivate: [AuthGuardService],
        component: MonsterListComponent
    },
    {
      path: 'login',
      component: LoginComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
