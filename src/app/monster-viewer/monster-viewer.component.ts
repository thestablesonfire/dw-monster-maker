import { Component, OnInit, Input } from '@angular/core';
import { ActiveMonsterService } from '../services/active-monster/active-monster.service';
import { MonsterSizeService } from '../models/monster/size/monster-size.service';
import { WeaponTag } from '../models/monster/tag/weapon-tag/weapon-tag';
import { WeaponTagService } from '../models/monster/tag/weapon-tag/weapon-tag.service';
import { DamageValueService } from '../models/monster/damageValue/die-size/damage-value.service';
import { MonsterTagService } from '../models/monster/tag/monster-tag/monster-tag.service';
import { Monster } from '../models/monster/monster';

@Component({
  selector: 'app-monster-viewer',
  templateUrl: './monster-viewer.component.html',
  styleUrls: ['./monster-viewer.component.scss']
})
export class MonsterViewerComponent {
  @Input() activeMonster: Monster;

  constructor(
    private damageValueService: DamageValueService,
    private monsterTagService: MonsterTagService,
    private weaponTagService: WeaponTagService
  ) { }

  getActiveArmor(): string {
    const armor = this.activeMonster.getArmor();

    return armor || armor === 0 ? armor + ' Armor' : '';
  }

  getActiveAttackName(): string {
    return this.activeMonster.attackName ? this.activeMonster.attackName : '';
  }

  getActiveDamageValue(): string {
    const attackVal = this.activeMonster.getDamageValue(this.damageValueService, this.weaponTagService);

    return attackVal.getDamageString() === '(d0 damage)' ? '' : attackVal.getDamageString();
  }

  getActiveDescription() {
    return this.activeMonster.description ? this.activeMonster.description : '';
  }

  getActiveHP(): string {
    const monsterHP = this.activeMonster.getHP();

    return monsterHP ? monsterHP + ' HP' : '';
  }

  getActiveInstinct() {
    return this.activeMonster.instinct ? this.activeMonster.instinct : '';
  }

  getActiveMonsterTags(): string {
    return this.activeMonster.getMonsterTags(this.monsterTagService);
  }

  getActiveMoves() {
    return this.activeMonster.moves.length ? this.activeMonster.moves : [];
  }

  getActiveSpecialQuality() {
    return this.activeMonster.specialQuality ? this.activeMonster.specialQuality : '';
  }

  getActiveWeaponTags(): string {
    return this.activeMonster.getWeaponTags(this.weaponTagService);
  }
}
