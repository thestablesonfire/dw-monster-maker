import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import { MonsterListComponent } from './monster-list/monster-list.component';
import { MonsterMakerComponent } from './monster-maker/monster-maker.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { MonsterViewerComponent } from './monster-viewer/monster-viewer.component';
import { MovesComponent } from './moves/moves.component';
import { MonsterPickerComponent } from './monster-picker/monster-picker.component';
import { AddListDialogComponent } from './add-list-dialog/add-list-dialog.component';
import { DeleteListDialogComponent } from './delete-list-dialog/delete-list-dialog.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ng2-cookies';
import { AuthGuardService } from './auth/auth-guard.service';

@NgModule({
  declarations: [
    AddListDialogComponent,
    AppComponent,
    DeleteListDialogComponent,
    LoginComponent,
    MenuComponent,
    MonsterListComponent,
    MonsterMakerComponent,
    MonsterPickerComponent,
    MonsterViewerComponent,
    MovesComponent,
  ],
  imports: [
    AngularMaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    CookieService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
