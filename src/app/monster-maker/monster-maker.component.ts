import { Component, OnInit } from '@angular/core';
import { Monster } from '../models/monster/monster';
import { MonsterSizeService } from '../models/monster/size/monster-size.service';
import { MonsterSize } from '../models/monster/size/monster-size';
import { MonsterGroupSize } from '../models/monster/group-size/monster-group-size';
import { MonsterGroupSizeService } from '../models/monster/group-size/monster-group-size.service';
import { MonsterDefense } from '../models/monster/defense/monster-defense';
import { MonsterDefenseService } from '../models/monster/defense/monster-defense.service';
import { MonsterTag } from '../models/monster/tag/monster-tag/monster-tag';
import { MonsterTagService } from '../models/monster/tag/monster-tag/monster-tag.service';
import { ActiveMonsterService } from '../services/active-monster/active-monster.service';
import { WeaponTag } from '../models/monster/tag/weapon-tag/weapon-tag';
import { WeaponTagService } from '../models/monster/tag/weapon-tag/weapon-tag.service';
import { ModifierOperation } from '../models/monster/tag/modifier/modifier-operation.enum';
import { MonsterPickerComponent } from '../monster-picker/monster-picker.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MonsterList } from '../monster-list/model/monster-list';
import { MonsterListService } from '../services/monster-list/monster-list.service';

@Component({
    selector: 'app-monster-maker',
    templateUrl: './monster-maker.component.html',
    styleUrls: ['./monster-maker.component.scss']
})
export class MonsterMakerComponent implements OnInit {
    public activeMonster: Monster;
    public activeMonsterList: MonsterList;
    public isAddingMove: boolean;
    public isAddingMonsterTag: boolean;
    public isAddingWeaponTag: boolean;
    public modifierPlus: ModifierOperation;
    public monsterDefenses: MonsterDefense[];
    public monsterGroupSizes: MonsterGroupSize[];
    public monsterSizes: MonsterSize[];
    public monsterTags: MonsterTag[];
    public newMove: string;
    public newMonsterTag: MonsterTag;
    public newWeaponTag: WeaponTag;
    public newWeaponTagModifier: number;
    public JSON = JSON;
    public weaponTags: WeaponTag[];

    constructor(
        private bottomSheet: MatBottomSheet,
        private activeMonsterService: ActiveMonsterService,
        private monsterDefenseService: MonsterDefenseService,
        private monsterGroupSizeService: MonsterGroupSizeService,
        private monsterListService: MonsterListService,
        private monsterSizeService: MonsterSizeService,
        private monsterTagService: MonsterTagService,
        private weaponTagService: WeaponTagService,
    ) {
        this.setNewActiveMonster();
        this.activeMonster.name = 'Beetle';
        this.activeMonster.description = 'A little, shelled bug';
        this.activeMonsterList = monsterListService.getActiveMonsterList();
        this.modifierPlus = ModifierOperation.PLUS;
        this.monsterDefenses = this.monsterDefenseService.getDefenses();
        this.monsterGroupSizes = this.monsterGroupSizeService.getGroupSizes();
        this.monsterSizes = this.monsterSizeService.getSizes();
        this.monsterTags = this.monsterTagService.getAllMonsterTags();
        this.isAddingMonsterTag = false;
        this.isAddingMove = false;
        this.isAddingWeaponTag = false;
        this.newMonsterTag = null;
        this.newMove = '';
        this.newWeaponTag = null;
        this.weaponTags = this.weaponTagService.getAllWeaponTags();
    }

    ngOnInit() {
      this.activeMonsterService.activeMonsterChangedEvent.subscribe(() => {
        this.newActiveMonster();
      });
    }

    addMonsterTag() {
        this.activeMonster.addMonsterTag(this.newMonsterTag);
        this.cancelAddMonsterTag();
    }

    addMove() {
        this.activeMonster.addMove(this.newMove);
        this.cancelAddMove();
    }

    addWeaponTag() {
        this.newWeaponTag.updateModifierValue(this.newWeaponTagModifier);
        this.activeMonster.addWeaponTag(this.newWeaponTag);
        this.cancelAddWeaponTag();
    }

    cancelAddMove() {
        this.newMove = '';
        this.isAddingMove = false;
    }

    cancelAddMonsterTag() {
        this.newMonsterTag = null;
        this.isAddingMonsterTag = false;
    }

    cancelAddWeaponTag() {
        this.newWeaponTag = null;
        this.isAddingWeaponTag = false;
    }

    deleteMonsterTag(tagIndex: number) {
        this.activeMonster.deleteMonsterTag(tagIndex);
    }

    deleteMove(index: number) {
        this.activeMonster.deleteMove(index);
    }

    deleteWeaponTag(index: number) {
        this.activeMonster.deleteWeaponTag(index);
    }

    getMonsterLists() {
      return this.monsterListService.getMonsterLists();
    }

    saveMonsterToList() {
      this.activeMonsterService.saveMonsterToList();
      this.setNewActiveMonster();
    }

    setNewActiveMonster() {
      this.activeMonster = this.activeMonsterService.setNewActiveMonster();
    }

    showMonsterPicker() {
      this.bottomSheet.open(MonsterPickerComponent);
    }

    showMonsterTagForm() {
        this.isAddingMonsterTag = true;
    }

    showMoveForm() {
        this.isAddingMove = true;
    }

    showWeaponTagForm() {
        this.isAddingWeaponTag = true;
    }

    private newActiveMonster() {
      console.log(this.activeMonsterService.getActiveMonster());
      this.activeMonster = this.activeMonsterService.getActiveMonster();
    }
}
