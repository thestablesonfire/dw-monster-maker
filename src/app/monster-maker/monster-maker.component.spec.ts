import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonsterMakerComponent } from './monster-maker.component';

describe('MonsterMakerComponent', () => {
  let component: MonsterMakerComponent;
  let fixture: ComponentFixture<MonsterMakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonsterMakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonsterMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
